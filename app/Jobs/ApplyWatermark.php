<?php

namespace App\Jobs;

use App\Models\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Spatie\Image\Image as SpatieImage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;


class ApplyWatermark implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    private $announcement_image_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id = $announcement_image_id;
    }

    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $i = Image::find($this->announcement_image_id);
        if (!$i) {
            return;
        }

        $srcPath = storage_path('app/public/' . $i->path);


            $image = SpatieImage::load($srcPath);

            $image->watermark(base_path('resources/img/smile1.png'))
                ->watermarkPosition('top-left')
                /* ->watermarkPadding($bounds[0][0], $bounds[0][1]) */
                ->watermarkWidth(20, Manipulations::UNIT_PERCENT)
                ->watermarkHeight(20, Manipulations::UNIT_PERCENT)
                ->watermarkFit(Manipulations::FIT_STRETCH);
                

            $image->save($srcPath);
        }
        
        
}
