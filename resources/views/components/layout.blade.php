<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content ="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    <title>{{$title ?? 'Emezon'}}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400&display=swap" rel="stylesheet">
    @livewireStyles

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    {{$style ?? ''}}
  </head>
  <body>

    <x-nav />

    <div class="min-vh-100 text-light">
        {{$slot}}
    </div>

    <x-footer />
    @livewireScripts

    <script src="{{asset('js/app.js')}}"></script>
    {{$script ?? ''}}
   
  </body>
</html>