<div class ="container-fluid mt-4 p-5 bg-dark text-light">
    <div class="row">
        <div class="col-12 text-center">
            <p>Emezon</p>
            <p>Vuoi lavorare con noi?</p>
            <p>Registrati e clicca qui!</p>
            <a href="{{route('become.revisor')}}" class="btn btn-warning text-black shadow my-3">Diventa revisore</a>
        </div>
    </div>
</div>
