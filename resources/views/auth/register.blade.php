<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">Registrati</h1>
                    <form action="{{route('register')}}" method="POST" >
                        @csrf

                        <div class="mb-3">
                            <label for="name" class="form-label">Nome completo</label>
                            <input name="name" type="text" class="form-control" id="name" aria-describedby="name">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail" class="form-label">Email Address</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text"> We'll never share email with anyone else. </div>
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password</label>
                            <input name="password" type="password" class="form-control" id="exampleInputPassword1" aria-describedby="emailHelp">
                        </div>

                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Password</label>
                            <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
                        </div>

                        <button type="submit" class="btn btn-primary">
                            Registrati
                        </button>
                    </form>
            </div>
        </div>
    </div>
</x-layout>