<x-layout>
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card text-dark">
                    <div class="card-header"><h1 class="text-center">Login</h1></div>
                        <div class="card-body">
                        <form action="{{route('login')}}" method="POST" >
                            @csrf

                            <div class="form-group row">
                                <label for="exampleInputEmail" class="col-sm-4 col-form-label text-md-right">Email Address</label>
                                <div class="col-md-6">
                                    <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="exampleInputEmail" aria-describedby="emailHelp" required autofocus>
                                    <div id="emailHelp" class="form-text"> We'll never share email with anyone else. </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleInputPassword1" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="exampleInputPassword1" aria-describedby="emailHelp" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-4">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>