<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1>{{ __('ui.welcome') }}</h1>
                <p class="h2 my-2 fw-bold">{{ __('ui.aliAnnouncements') }}</p>
                <div class="row ">
                    @foreach ($announcements as $announcement)
                        <div class="col-12 col-md-4 my-4">
                            <div class="card-deck">
                                <div class="card text-black bg-warning">
                                    <img src="{{ !$announcement->images()->get()->isEmpty()? $announcement->images()->first()->getUrl(400, 300): 'https://picsum.photos/200' }}"
                                        class="card-img-top p-3 rounded" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $announcement->title }}</h5>
                                        <p class="card-text">{{ $announcement->body }}</p>
                                        <p class="card-text">{{ $announcement->price }}</p>
                                        <div class="button">
                                        <a href="{{ route('announcements.show', compact('announcement')) }}"
                                            class="btn btn-primary shadow">Visualizza</a>
                                        <a href="{{ route('categoryShow', ['category' => $announcement->category]) }}"
                                            class="my-2 border-top pt-2 border-dark card-link shadow btn btn-success">Categoria:
                                            {{ $announcement->category->name }}</a>
                                        </div>
                                        <p class="card-footer">Pubblicato il:
                                            {{ $announcement->created_at->format('d/m/Y') }} <br> Autore:
                                            {{ $announcement->user->name ?? '' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-layout>
